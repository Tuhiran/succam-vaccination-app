<?php
use Carbon\Carbon as Carbon;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('admins')->insert([
            'name' => 'John Doe',
            'email' => 'admin@admin.com',
            'password' => Hash::make('1234'),
            'remember_token' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

}
