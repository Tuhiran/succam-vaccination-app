<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubtopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_topics', function (Blueprint $table) {
            $table->id();
            $table->integer('topic_id')->unsigned();
            $table->string('name');
            $table->string('description')->nullable();            
            $table->enum('status',['Y','N'])->default('Y');
            $table->timestamps();

            /*$table->foreign('topic_id')
            ->references('id')
            ->on('topics')
            ->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subtopics');
    }
}
