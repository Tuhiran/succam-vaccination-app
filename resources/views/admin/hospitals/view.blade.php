@extends('admin.layouts.app', ['activePage' => 'hospitals', 'titlePage' => __('Hospital List')])
@section('title', 'Hospital List')
@section('content')
    <div class="content">

        <!-- Create Staff Modal -->
        <div class="modal fade" id="staffCreateModal" tabindex="-1" role="dialog" aria-labelledby="supTopicUpdateLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Staff</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" autocomplete="off" class="form-horizontal" id="dev_staff_create_form">
                            @csrf
                            <input type="hidden" name="hospital" value="{{ $model->id }}" id="subtopic_id">
                            <div class="card ">
                                <div class="card-body ">
                                    <div class="row">
                                        <label class="col-sm-5 col-form-label">{{ __('Staff Name') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                                <input class="form-control" name="name" id="name" type="text" placeholder="{{ __('Staff Name') }}" value="{{ old('name') }}" required="true" aria-required="true" />
                                                <span id="dev_error_name" class="dev-error" for="input-name"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-5 col-form-label">{{ __('Staff Email') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                                <input class="form-control" name="email" id="email" type="text" placeholder="{{ __('Staff Email') }}" value="{{ old('email') }}" required="true" aria-required="true" />
                                                <span id="dev_error_email" class="dev-error" for="input-name"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-5 col-form-label" for="input-password">{{ __('PIN') }}</label>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                                <input class="form-control" name="password" id="input_password" type="text" placeholder="{{ __('PIN') }}" value="" required />
                                                <span id="dev_error_password" class="dev-error" for="input-password"></span>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <button type="button" class="btn btn-button btn-primary" onclick="generatePin()">{{ __('Generate') }}</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-5 col-form-label">{{ __('Status') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                                <select class="form-control" name="status" id="status" required="true" aria-required="true">
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer ml-auto mr-auto">
                                    <button type="button" class="btn btn-primary dev-create-staff">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Update Staff Modal -->
        <div class="modal fade" id="staffUpdateModal" tabindex="-1" role="dialog" aria-labelledby="supTopicUpdateLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Staff</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form autocomplete="off" class="form-horizontal" method="post" id="dev_staff_update_form">
                            @csrf
                            <input type="hidden" name="staff" value="" id="staff_id">
                            <input type="hidden" name="hospital" value="{{ $model->id }}">
                            <div class="card ">
                                <div class="card-body ">
                                    <div class="row">
                                        <label class="col-sm-5 col-form-label">{{ __('Staff Name') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                                <input class="form-control" name="name" id="edit_staff_name" type="text" placeholder="{{ __('Staff Name') }}" value="" required="true" aria-required="true" />
                                                <span id="dev_edit_error_name" class="" for="input-name"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-5 col-form-label">{{ __('Staff Email') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                                <input class="form-control" name="email" id="edit_staff_email" type="text" placeholder="{{ __('Staff Email') }}" value="" required="true" aria-required="true" />
                                                <span id="dev_edit_error_email" class="" for="input-email"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-5 col-form-label" for="input-password">{{ __('PIN') }}</label>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="edit_staff_password" type="text" placeholder="{{ __('PIN') }}" value="" required />
                                                <span id="edit_staff_passowrd_error" class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <button type="button" class="btn btn-button btn-primary" onclick="generatePin()">{{ __('Generate') }}</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-5 col-form-label">{{ __('Status') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('status') ? ' has-danger' : '' }}">
                                                <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" id="edit_staff_status" required="true" aria-required="true">
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                                @if ($errors->has('status'))
                                                    <span id="edit_staff_status_error" class="error text-danger" for="input-name">{{ $errors->first('status') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer ml-auto mr-auto">
                                    <button type="button" class="btn btn-primary dev-update-staff">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table" id="user_detail_table">
                                            <tbody>
                                                <tr>
                                                    <a href="{{ route('edit_hospital', $model->id) }}" class="btn btn-success">Update</a>
                                                </tr>
                                                <tr>
                                                    <th>{{ __('Hospital Name') }} </th>
                                                    <td>{{ $model->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{ __('Hospital Region') }} </th>
                                                    <td>{{ $model->region->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{ __('Hospital Department') }} </th>
                                                    <td>{{ $model->department->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{ __('City') }} </th>
                                                    <td>{{ $model->city->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{ __('Status') }} </th>
                                                    <td>{{ $model['status'] == 'A' ? 'Active' : 'Inactive' }}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{ __('Registered On') }} </th>
                                                    <td>{{ date('d M Y H:i', strtotime($model['created_at'])) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="text-right">
                                <a href="javascript:void(0);" class="btn btn-primary dev-add-staff" data-toggle="modal" data-target="#staffCreateModal">Add staff to this hospital</a>
                            </div>

                            <div class="table-responsive">
                                <table class="table" id="subtopic_list_table">
                                    <thead class="text-primary">
                                        <th>{{ __('Staff name') }} </th>
                                        <th>{{ __('Staff Email') }} </th>
                                        <th class="text-center">{{ __('Status') }} </th>
                                        <th class="text-center">{{ __('Registered On') }} </th>
                                        <th class="text-center">{{ __('Action') }} </th>
                                    </thead>
                                    <tbody>
                                        @foreach ($model->staffs as $staff)
                                            <tr>
                                                <td>{{ $staff->name }}</td>
                                                <td class="">{{ $staff->email }}</td>
                                                <td class="text-center">{{ $staff->status == 1 ? 'Active' : 'Inactive' }}</td>
                                                <td class="text-center">{{ date('d M Y H:i', strtotime($staff->created_at)) }}</td>
                                                <td class="text-center">
                                                    <a href="javascript:void(0)" class="dev-staff-update" data-id="{{ $staff->id }}"><span class="material-icons">edit</span></a>
                                                    <a href="javascript:void(0)" class="dev-staff-delete" data-id="{{ $staff->id }}"><span class="material-icons" style="color: #ff0000">delete_forever</span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('customScripts')
    <script>
        $(document).ready(function() {
            $('#subtopic_list_table').DataTable({
                "order": []
            });
            generatePin();
        });

        $('body').on('click', '.dev-create-staff', function() {
            $('body').find('.dev-error').html('').removeClass('error text-danger');
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: $('body').find('#dev_staff_create_form').serialize(),
                url: "{{ route('create_staff') }}",
                success: function(data) {
                    if (data.status == 'success') {
                        $('body').find('#staffCreateModal').modal('hide');
                        $.alert({
                            title: '<h3 style="font-weight:500;"><span class="material-icons" style="color:#4caf50;">check</span>&nbsp;&nbsp;Success</h3>',
                            content: '<h5>Staff Created.</h5>',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                ok: function() {
                                    location.href = "{{ route('view_hospital', ['id' => $model->id]) }}";
                                }
                            }
                        });
                    }
                },
                error: function(data) {
                    $.each(data.responseJSON.errors, function(index, value) {
                        if (index === 'password') {
                            $('body').find('#dev_error_' + index).html('PIN is required').addClass('error text-danger');
                        } else {
                            $('body').find('#dev_error_' + index).html(value).addClass('error text-danger');
                        }
                    });
                }
            });
        });

        $('body').on('click', '.dev-staff-update', function() {
            var staffId = $(this).attr('data-id');
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: "json",
                data: {
                    'id': staffId,
                },
                url: "{{ route('get_user_detail') }}",
                success: function(data) {
                    if (data.status == 'success') {
                        $("body").find('#staffUpdateModal #staff_id').val(staffId);
                        $("body").find('#staffUpdateModal #edit_staff_name').val(data.user.name);
                        $("body").find('#staffUpdateModal #edit_staff_email').val(data.user.email);
                        $("body").find('#staffUpdateModal #edit_staff_password').val(data.user.pin);
                        $("body").find('#staffUpdateModal #edit_staff_status').val(data.user.status);
                        $("body").find("#staffUpdateModal").modal('show');
                    } else if (data.error == 'authentication_expired') {
                        location.href = "{{ route('admin-login') }}";
                    }
                }
            });
        });

        $('body').on('click', '.dev-update-staff', function() {
            $('body').find('.dev-error').html('').removeClass('error text-danger');
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: $('body').find('#dev_staff_update_form').serialize(),
                url: "{{ route('update_staff') }}",
                success: function(data) {
                    if (data.status == 'success') {
                        $('body').find('#staffCreateModal').modal('hide');
                        $.alert({
                            title: '<h3 style="font-weight:500;"><span class="material-icons" style="color:#4caf50;">check</span>&nbsp;&nbsp;Success</h3>',
                            content: '<h5>Staff Updated.</h5>',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                ok: function() {
                                    location.href = "{{ route('view_hospital', ['id' => $model->id]) }}";
                                }
                            }
                        });
                    }
                },
                error: function(data) {
                    $.each(data.responseJSON.errors, function(index, value) {
                        if (index === 'password') {
                            $('body').find('#dev_edit_error_' + index).html('PIN is required').addClass('error text-danger');
                        } else {
                            $('body').find('#dev_edit_error_' + index).html(value).addClass('error text-danger');
                        }
                    });
                }
            });
        });

        $('body').on('click', '.dev-staff-delete', function() {
            var staffId = $(this).attr('data-id');
            $.confirm({
                title: '<h3 style="font-weight:500;"><span class="material-icons" style="color:#d58512;">warning</span>&nbsp;&nbsp;Alert</h3>',
                content: '<h5>Are you sure you want to delete this staff?</h5>',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    confirm: function() {
                        $.ajax({
                            type: "post",
                            headers: {
                                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType: "json",
                            data: {
                                'id': staffId
                            },
                            url: "{{ route('delete_staff') }}",
                            success: function(message) {
                                if (message.status == 'success') {
                                    location.reload();
                                } else if (message.error == 'authentication_expired') {
                                    location.href = "{{ route('admin-login') }}";
                                }
                            }
                        });
                    },
                    cancel: function() {},
                }
            });
        });

        function generatePin() {
            var rString = randomString(6, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            $("#input_password").val(rString);
            console.log(rString);
        }

        function generateEditPin() {
            var rString = randomString(6, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            $("#edit_input_password").val(rString);
            console.log(rString);
        }

        function randomString(length, chars) {
            var result = '';
            for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
            return result;
        }

    </script>
@endsection
