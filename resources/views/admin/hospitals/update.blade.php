@extends('admin.layouts.app', ['activePage' => 'hospitals', 'titlePage' => __('Update Hospital')])
@section('title', 'Update Hospital')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('edit_hospital', $model['id']) }}" autocomplete="off" class="form-horizontal" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card ">
                            <div class="card-body ">
                                @if (session('error'))
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="alert alert-danger">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <i class="material-icons">close</i>
                                                </button>
                                                <span>{{ session('error') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Hospital Name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('days') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" type="text" placeholder="{{ __('Hospital Name') }}" value="{{ $model->name }}" required="true" aria-required="true" />
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Region') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('region_id') ? ' has-danger' : '' }}">
                                            <input type="hidden" value="{{ old('region_id') != '' ? old('region_id') : $model->region_id }}" id="region_old_hidden_value">
                                            <select class="form-control{{ $errors->has('region_id') ? ' is-invalid' : '' }}" name="region_id" id="region_id" aria-required="true">
                                                <option value="" disabled selected>Choose Region</option>
                                                @foreach ($regionList as $region)
                                                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('region_id'))
                                                <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('region_id') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Department') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('department_id') ? ' has-danger' : '' }}">
                                            <input type="hidden" value="{{ old('department_id') != '' ? old('department_id') : $model->department_id }}" id="department_old_hidden_value">
                                            <select class="form-control{{ $errors->has('department_id') ? ' is-invalid' : '' }}" name="department_id" id="department_id" aria-required="true">
                                                <option value="" disabled selected>Choose Department</option>
                                                @foreach ($departmentList as $department)
                                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('department_id'))
                                                <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('department_id') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('City') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('city_id') ? ' has-danger' : '' }}">
                                            <input type="hidden" value="{{ old('city_id') != '' ? old('city_id') : $model->city_id }}" id="city_old_hidden_value">
                                            <select class="form-control{{ $errors->has('city_id') ? ' is-invalid' : '' }}" name="city_id" id="city_id" aria-required="true">
                                                <option value="" disabled selected>Choose City</option>
                                                @foreach ($cityList as $city)
                                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('city_id'))
                                                <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('city_id') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Status') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('status') ? ' has-danger' : '' }}">
                                            <input type="hidden" value="{{ old('status')!= '' ? old('status') : $model->status}}" id="status_hidden_value">
                                            <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" id="status" required="true" aria-required="true">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                            @if ($errors->has('status'))
                                                <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('status') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('customScripts')
    <script>
        $(document).ready(function() {
            var regionOld = $("#region_old_hidden_value").val();
            $("#region_id").val(regionOld);
            var departmentOld = $("#department_old_hidden_value").val();
            $("#department_id").val(departmentOld);
            var cityOld = $("#city_old_hidden_value").val();
            $("#city_id").val(cityOld);
            var slotStatus = $("#status_hidden_value").val();
            $("#status").val(slotStatus);

        });

    </script>
@endsection
