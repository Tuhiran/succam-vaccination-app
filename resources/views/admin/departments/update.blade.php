@extends('admin.layouts.app', ['activePage' => 'departments', 'titlePage' => __('Update Department')])
@section('title', 'Update Department')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('edit_department', $model['id']) }}" autocomplete="off" class="form-horizontal" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card ">
                            <div class="card-body ">
                                @if (session('error'))
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="alert alert-danger">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <i class="material-icons">close</i>
                                                </button>
                                                <span>{{ session('error') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Department Name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" type="text" placeholder="{{ __('Department Name') }}" value="{{ $model->name }}" required="true" aria-required="true" />
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Status') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('status') ? ' has-danger' : '' }}">
                                            <input type="hidden" value="{{ $model['status'] }}" id="status_hidden_value">
                                            <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" id="status" required="true" aria-required="true">
                                                <option value="" disabled selected>Choose your option</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                            @if ($errors->has('status'))
                                                <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('status') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('customScripts')
    <script>
        $(document).ready(function() {
            var slotStatus = $("#status_hidden_value").val();
            $("#status").val(slotStatus);
        });

    </script>
@endsection
