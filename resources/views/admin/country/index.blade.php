@extends('admin.layouts.app', ['activePage' => 'country', 'titlePage' => __('Country List')])
@section('title', 'Country List')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="country_list_table">
                                <thead class="text-primary">
                                    <th>{{ __('Country name') }} </th>
                                    <th>{{ __('Country ISO') }} </th>
                                    <th>{{ __('Country ISO3') }} </th>
                                    <th class="text-center">{{ __('Action') }}</th>
                                </thead>
                                <tbody>
                                    @foreach($modelData as $model)
                                    <tr>
                                        <td>{{ucfirst(str_replace('_' , ' ', $model->nicename))}}</td>
                                        <td>{{$model->iso}}</td>
                                        <td>{{$model->iso3}}</td>
                                        <td class="text-center">
                                            @if($model->status == 0)
                                                <a href="{{ route('country_update',['id' => $model->id, 'status' => '1']) }}" class="btn btn-success"><span class="material-icons" style="color: #006909">check_circle</span> Activate</a>
                                            @elseif($model->status == 1)
                                                <a href="{{ route('country_update',['id' => $model->id,'status' => '0']) }}" class="btn btn-warning"><span class="material-icons" style="color: #473f3f67; font-weight: 600;">block</span> Inactivate</a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('customScripts')
<script>
    $(document).ready(function () {
        $('#country_list_table').DataTable({
            "order": []
        });
    });
</script>
@endsection