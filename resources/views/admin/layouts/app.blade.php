<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Sucam Vaccination - @yield('title')</title>
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('admin_assets') }}/img/apple-icon.png">
        <link rel="icon" type="image/png" href="">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <!-- CSS Files -->
        <link href="{{ asset('admin_assets') }}/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/css/fileinput.min.css">        
        <link href="{{ asset('admin_assets') }}/demo/demo.css" rel="stylesheet" />
        <!--<link href="{{ asset('admin_assets') }}/css/lightbox.min.css" rel="stylesheet" />-->
        <link href="{{ asset('admin_assets') }}/css/developer-style.css" rel="stylesheet" />
    </head>
    <body class="{{ $class ?? '' }}">
        @auth()
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        @include('admin.layouts.page_templates.auth')
        @endauth
        @guest()
        @include('admin.layouts.page_templates.guest')
        @endguest
        <!--   Core JS Files   -->
        <!--<script src="{{ asset('admin_assets') }}/js/core/jquery.min.js"></script>-->
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="{{ asset('admin_assets') }}/js/core/popper.min.js"></script>
        <script src="{{ asset('admin_assets') }}/js/core/bootstrap-material-design.min.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/moment.min.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/sweetalert2.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/jquery.validate.min.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/jquery.bootstrap-wizard.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/bootstrap-selectpicker.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/bootstrap-datetimepicker.min.js"></script>
        <!--<script src="{{ asset('admin_assets') }}/js/lightbox-plus-jquery.min.js"></script>-->
        <script src="{{ asset('admin_assets') }}/js/plugins/jquery.dataTables.min.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/bootstrap-tagsinput.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/jasny-bootstrap.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/fullcalendar.min.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/jquery-jvectormap.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/nouislider.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/arrive.min.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/chartist.min.js"></script>
        <script src="{{ asset('admin_assets') }}/js/plugins/bootstrap-notify.js"></script>        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>        
        <script src="{{ asset('admin_assets') }}/js/material-dashboard.js?v=2.1.1" type="text/javascript"></script>        
        <script src="{{ asset('admin_assets') }}/demo/demo.js"></script>
        <script src="{{ asset('admin_assets') }}/js/settings.js"></script>
        @stack('js')
        @yield('customScripts')
    </body>
</html>