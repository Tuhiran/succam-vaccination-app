<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('public/admin_assets') }}/img/sidebar-1.jpg">
    <div class="logo">
        <a href="{{ env('APP_URL')}}" class="simple-text logo-normal">
            {{ __('Sucam Vaccination') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <i class="material-icons">dashboard</i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'hospitals' ? ' active' : '' }}">
                <a class="nav-link" href="{{route('hospital_list')}}">
                    <span class="material-icons">local_hospital</span>
                    <span class="sidebar-normal">{{ __('Hospital List') }} </span>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'cities' ? ' active' : '' }}">
                <a class="nav-link" href="{{route('city_list')}}">
                    <span class="material-icons">room</span>
                    <span class="sidebar-normal">{{ __('City List') }} </span>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'departments' ? ' active' : '' }}">
                <a class="nav-link" href="{{route('department_list')}}">
                    <span class="material-icons">work_outline</span>
                    <span class="sidebar-normal">{{ __('Department List') }} </span>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'id_types' ? ' active' : '' }}">
                <a class="nav-link" href="{{route('id_type_list')}}">
                    <span class="material-icons">perm_identity</span>
                    <span class="sidebar-normal">{{ __('ID Proof Type List') }} </span>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'injection_sites' ? ' active' : '' }}">
                <a class="nav-link" href="{{route('injection_site_list')}}">
                    <span class="material-icons">indeterminate_check_box</span>
                    <span class="sidebar-normal">{{ __('Injection Site List') }} </span>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'regions' ? ' active' : '' }}">
                <a class="nav-link" href="{{route('region_list')}}">
                    <span class="material-icons">share_location</span>
                    <span class="sidebar-normal">{{ __('Region List') }} </span>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'vaccine_manufacturers' ? ' active' : '' }}">
                <a class="nav-link" href="{{route('vaccine_manufacturer_list')}}">
                    <span class="material-icons">precision_manufacturing</span>
                    <span class="sidebar-normal">{{ __('Vaccine Manufacturer List') }} </span>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'vaccine_types' ? ' active' : '' }}">
                <a class="nav-link" href="{{route('vaccine_type_list')}}">
                    <span class="material-icons">api</span>
                    <span class="sidebar-normal">{{ __('Vaccine Type List') }} </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <span class="material-icons">logout</span>
                    <span class="sidebar-normal">{{ __('Logout') }} </span>
                </a>
            </li>
        </ul>
    </div>
</div>
