@extends('admin.layouts.app', ['activePage' => 'regions', 'titlePage' => __('Region List')])
@section('title', 'Region List')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="text-right">
                            <a href="{{route('create_region')}}" class="btn btn-primary">Add</a>
                        </div>
                        <div class="table-responsive">
                            <table class="table" id="region_list_table">
                                <thead class="text-primary">
                                    <th>{{ __('Region Name') }} </th>
                                    <th class="text-center">{{ __('Status') }} </th>
                                    <th class="text-center">{{ __('Registered On') }} </th>
                                    <th class="text-center">{{ __('Action') }} </th>
                                </thead>
                                <tbody>
                                    @foreach($regionList as $model)
                                    <tr>
                                        <td>{{$model->name}}</td>
                                        <td class="text-center">{{$model->status == 1 ? 'Active' : 'Inactive'}}</td>
                                        <td class="text-center">{{date('d M Y H:i', strtotime($model->created_at))}}</td>
                                        <td class="text-center">
                                            <a href="{{route('edit_region', $model->id)}}"><span class="material-icons">edit</span></a>
                                            <a href="javascript:void(0)" class="dev-region-delete" data-id="{{ $model->id }}"><span class="material-icons" style="color: #ff0000">delete_forever</span></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('customScripts')
<script>
    $(document).ready(function() {
        
        $('#region_list_table').DataTable({
             "order": []
        });
        
        $('.dev-region-delete').on('click', function() {
            var injectionSiteId = $(this).attr('data-id');
            $.confirm({
                title: '<h3 style="font-weight:500;"><span class="material-icons" style="color:#d58512;">warning</span>&nbsp;&nbsp;Alert</h3>',
                content: '<h5>Are you sure you want to delete this region?</h5>',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    confirm: function() {
                        $.ajax({
                            type: "POST",
                            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                            dataType: "json",
                            data: {'id': injectionSiteId},
                            url: "{{route('delete_region')}}",
                            success: function (message) {
                                if (message.success) {
                                    window.location = message.url;
                                }
                            }
                        });
                    },
                    cancel: function() {},
                }
            });
        })
    });
</script>
@endsection