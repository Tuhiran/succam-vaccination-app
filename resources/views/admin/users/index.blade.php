@extends('admin.layouts.app', ['activePage' => 'hospitals', 'titlePage' => __('Hospital List')])
@section('title', 'Hospital List')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <a href="{{ route('create_hospital')}}" style="float: right;"><button type="button" class="btn btn-primary">{{ __('Add') }}</button></a>

                        <div class="table-responsive">
                            <table class="table" id="user_list_table">
                                <thead class=" text-primary">
                                <th>{{ __('Hospital Name') }} </th>
                                <th>{{ __('Hospital ID') }} </th>
                                <th>{{ __('Created At') }}</th>
                                <th>{{ __('Action') }} </th>
                                </thead>
                                <tbody>
                                    @foreach($userList as $user)
                                    <tr>
                                        <td>{{$user['name']}}</td>
                                        <td>{{$user['email']}}</td>
                                       
                                        <td>{{date('d M Y H:i', strtotime($user['created_at']))}}</td>
                                        <td>

                                            <a href="{{route('edit_hospital', $user['id'])}}"><span class="material-icons">edit</span></a>

                                            <a href="{{route('user_detail', $user['id'])}}"><span class="material-icons">visibility</span></a>
                                            
                                        </td>


                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="giveaway_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
      </div>
    </div>
  </div>


@endsection
@section('customScripts')
<script>
    $(document).ready(function () {
        $('#user_list_table').DataTable({
             "order": []
        });

        

    });
</script>
@endsection