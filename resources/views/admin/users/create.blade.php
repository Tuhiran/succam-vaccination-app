@extends('admin.layouts.app', ['activePage' => 'hospitals', 'titlePage' => __('Hospital Create')])
@section('title', 'Create Hospital')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <form method="post" action="{{ route('create_hospital') }}" autocomplete="off" class="form-horizontal" method="post" enctype="multipart/form-data">
                @if($errors->any())
                    <div class="alert alert-danger" style="margin-top: 30px;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        @foreach($errors->all() as $error)
                        <p>{!! $error !!}</p>
                        @endforeach
                    </div>
                @endif
                    @csrf
                    <div class="card ">
                        <div class="card-body ">
                            @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Hospital Name') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" type="text" placeholder="{{ __('Hospital name') }}" value="{{ old('name') }}" aria-required="true"/>
                                        @if ($errors->has('days'))
                                        <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Hospital ID') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" type="text" placeholder="{{ __('Hospital ID') }}" value="{{ old('email') }}" aria-required="true"/>
                                        @if ($errors->has('days'))
                                        <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            
                            <div class="row">
                              <label class="col-sm-2 col-form-label" for="input-password">{{ __('PIN') }}</label>
                              <div class="col-sm-5">
                                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                  <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="input-password" type="text" placeholder="{{ __('PIN') }}" value="" required />
                                  @if ($errors->has('password'))
                                    <span id="password-error" class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>
                                  @endif
                                </div>
                              </div>

                            <div class="col-sm-5">
                                <button type="button" class="btn btn-button" onclick="generatePin()">{{ __('Generate') }}</button>
                            </div>

                            </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto" style="padding-left: 280px;">
                            <a href="{{ route('hospital_list')}}"><button type="button" class="btn btn-primary">{{ __('Back') }}</button></a>
                            <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>

                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('customScripts')
<script>

    function generatePin(){

        var rString = randomString(8, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        $("#input-password").val(rString);
        console.log(rString);
    }

    function randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }

</script>
@endsection