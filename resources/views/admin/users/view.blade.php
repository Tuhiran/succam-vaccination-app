@extends('admin.layouts.app', ['activePage' => 'hospitals', 'titlePage' => __('Hospital Detail')])
@section('title', 'Hospital Detail')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <!-- <img class="img img-thumbnail dev-user-detail-img" src="{{asset('assets/upload/profile_image/'.$userDetail['profile_image'])}}" style="max-height: 285px;"> -->
                            </div>
                            <div class="col-md-9">
                                <div class="table-responsive">
                                    <table class="table" id="user_detail_table">
                                        <tbody>
                                            <tr><td></td></tr>
                                            <tr>
                                                <th>{{ __('Name') }} </th>
                                                <td>{{$userDetail['name']}}</td> 
                                            </tr>
                                           
                                            <tr>
                                                <th>{{ __('Hospital ID') }} </th>
                                                <td>{{$userDetail['email']}}</td>
                                            </tr>

                                            <tr>
                                                <th>{{ __('PIN') }} </th>
                                                <td>{{$userDetail['pin']}}</td>
                                            </tr>
                                            
                                            <tr><th>{{ __('Registered On') }} </th>
                                                <td>{{date('d M Y H:i', strtotime($userDetail['created_at']))}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <a href="{{ route('hospital_list')}}"><button type="button" class="btn btn-primary">{{ __('Back') }}</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection