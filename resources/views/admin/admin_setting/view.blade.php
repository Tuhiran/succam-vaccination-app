@extends('admin.layouts.app', ['activePage' => 'admin-setting', 'titlePage' => __('Admin Setting Detail')])
@section('title', 'Admin Detail')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <!--<div class="col-md-9">-->
                            <div class="table-responsive">
                                <table class="table dev-theory-detail-table" id="theory_detail_table">
                                    <tbody>
                                        <tr><td></td></tr>
                                        <tr>
                                            <th>{{ __('Feild Name') }} </th>
                                            <td>{{ucfirst(str_replace("_", " ", $model['field_name'])) }}</td>
                                        </tr>
                                        <tr>
                                            <th>{{ __('Field_value') }} </th>
                                            @if($model['field_type'] == "F")
                                            <td>                                                
                                                <video width="400" controls style="margin:10px">
                                                    <source src="{{asset('public/assets/upload/introductory_video/') . '/' . $model['field_value']}}" type="video/mp4">
                                                    Your browser does not support HTML video.
                                                </video>                                                
                                            </td>
                                            @else
                                            <td>{{$model['field_name'] }}</td>
                                            @endif
                                        </tr>
                                        @if($model['field_type'] == "F")
                                        <tr>
                                            
                                        </tr>
                                        @endif
                                        <tr>
                                            <th>{{ __('Status') }} </th>
                                            <td>{{$model['status'] == 'A' ? 'Active' : 'Inactive' }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--</div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection