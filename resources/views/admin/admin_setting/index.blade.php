@extends('admin.layouts.app', ['activePage' => 'admin-setting', 'titlePage' => __('Admin Settings List')])
@section('title', 'Admin Settings List')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="admin_setting_list_table">
                                <thead class="text-primary">
                                <th>{{ __('Field Name') }} </th>
                                <th>{{ __('Field Value') }} </th>
                                <th class="text-center">{{ __('Action') }} </th>
                                </thead>
                                <tbody>
                                    @foreach($modelData as $model)
                                    <tr>
                                        <td>{{ucfirst(str_replace('_' , ' ', $model['field_name']))}}</td>
                                        <td>{{$model['field_value']}}</td>
                                        <td class="text-center">
                                            @if($model['field_type'] == "F")
                                            <a href="{{route('view_admin_setting', $model['id'])}}"><span class="material-icons">visibility</span>
                                            @endIf
                                            <a href="{{route('update_admin_setting', $model['id'])}}"><span class="material-icons">edit</span></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('customScripts')
<script>
    $(document).ready(function () {
        $('#admin_setting_list_table').DataTable({
            "order": []
        });
    });
</script>
@endsection