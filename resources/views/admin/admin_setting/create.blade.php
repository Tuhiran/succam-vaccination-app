@extends('admin.layouts.app', ['activePage' => 'admin-setting', 'titlePage' => __('Admin Settings List')])
@section('title', 'Admin Settings List')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="{{ route('create_admin_setting') }}" autocomplete="off" class="form-horizontal" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card ">
                        <div class="card-body ">
                            @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Field Name') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('field_name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('field_name') ? ' is-invalid' : '' }}" name="field_name" id="field_name" type="text" placeholder="{{ __('Field Name') }}" value="{{ old('field_name') }}" aria-required="true"/>
                                        @if ($errors->has('days'))
                                        <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('field_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Field Type') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('field_type') ? ' has-danger' : '' }}">
                                        <select class="form-control{{ $errors->has('field_type') ? ' is-invalid' : '' }}" name="field_type" id="field_type" aria-required="true">
                                            <option value="" disabled selected>Choose your option</option>
                                            <option value="I">Input</option>
                                            <option value="F">File</option>
                                        </select>
                                        @if ($errors->has('field_type'))
                                        <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('field_type') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="input_type_field">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Field Value') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('field_value') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('field_value') ? ' is-invalid' : '' }}" name="input_type_field_value" id="input_type_field_value" type="text" placeholder="{{ __('Field Value') }}" value="{{ old('field_value') }}" aria-required="true"/>
                                            @if ($errors->has('field_value'))
                                            <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('field_value') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="file_type_field">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Field Value') }}</label>
                                    <div class="col-sm-7">
                                        <div class="custom-file pmd-custom-file-outline{{ $errors->has('field_value') ? ' has-danger' : '' }}">
                                            <input class="file{{ $errors->has('field_value') ? ' is-invalid' : '' }}" name="file_type_field_value" id="file_type_field_value" type="file" value="" aria-required="true"/>
                                            @if ($errors->has('field_value'))
                                            <span id="name-error" class="error text-danger dev-error" for="input-name">{{ $errors->first('field_value') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Video Thumbnail') }}</label>
                                    <div class="col-sm-7 form-group">
                                        <div class="file-field">
                                            <div class="btn btn-primary btn-sm float-left">
                                                <span>Browse</span>
                                                <input type="file" name="video_thumbnail" id="video_thumbnail">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Choose Cover Image Of File">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary" id="dev_btn_submit" >{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('customScripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/fileinput.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/themes/fa/theme.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.4.4/umd/popper.min.js" type="text/javascript"></script>
<script>

$("#file_type_field_value").fileinput({
    theme: 'fa',
    uploadUrl: '#',
    allowedFileExtensions: ['mkv', 'mp4', 'webm', 'avi', 'wmv', 'flv', 'swf'],
    overwriteInitial: false,
    maxFileSize: 20480,
    maxFilesNum: 1,
    showCaption: false,
    showRemove: true,
    showCancel: false,
    showUpload: false,
    slugCallback: function (filename) {
        return filename.replace('(', '_').replace(']', '_');
    },
    previewFileType: "image",
    browseClass: "btn btn-success btn-block"
});

$('#file_type_field_value').on('change', function () {
    var fileSize = this.files[0].size;
    var filesizeInKB = parseFloat(fileSize) / parseFloat(1024);
    if (filesizeInKB > 20480) {
        $('#dev_btn_submit').prop('disabled', true);
    } else {
        $('#dev_btn_submit').removeAttr('disabled');
    }
});

$(document).ready(function () {
    $('.input_type_field').css({"display": "none"});
    $('.file_type_field').css({"display": "none"});
    $('#field_type').change(function () {
        if ($(this).val() === 'I') {
            $('.input_type_field').css({"display": "block"});
            $('.file_type_field').css({"display": "none"});
        } else {
            $('.input_type_field').css({"display": "none"});
            $('.file_type_field').css({"display": "block"});
        }
    });


});
$('body').on('change', 'input[type="file"]', function (e) {
    var fileName = e.target.files[0].name;
    $(this).closest('div.file-field').find('input.file-path').val(fileName);
});
</script>
@endsection