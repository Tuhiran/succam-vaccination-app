<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

/* START :  Admin Routes */

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {

    Route::get('/', 'LoginController@loginPage')->name('admin-login');
    Route::post('login', 'LoginController@adminLogin')->name('login');
    Route::post('logout', 'LoginController@logout')->name('logout');
    Route::get('dashboard', 'HomeController@getDashboard')->name('dashboard');
    Route::get('index-admin-setting', 'AdminSettingController@index')->name('admin_setting_index');
    Route::get('create-admin-setting', 'AdminSettingController@create')->name('create_admin_setting');
    Route::post('create-admin-setting', 'AdminSettingController@create')->name('create_admin_setting');
    Route::get('update-admin-setting/{id}', 'AdminSettingController@update')->name('update_admin_setting');
    Route::post('update-admin-setting/{id}', 'AdminSettingController@update')->name('update_admin_setting');
    Route::get('view-admin-setting/{id}', 'AdminSettingController@view')->name('view_admin_setting');

    Route::get('hospital', 'HospitalController@list')->name('hospital_list');
    Route::get('hospital-detail/{id}', 'HospitalController@getUserDetail')->name('hospital_detail');
    Route::get('create-hospital', 'HospitalController@create')->name('create_hospital');
    Route::post('create-hospital', 'HospitalController@create')->name('create_hospital');
    Route::get('edit-hospital/{id}', 'HospitalController@edit')->name('edit_hospital');
    Route::post('edit-hospital/{id}', 'HospitalController@edit')->name('edit_hospital');
    Route::get('view-hospital/{id}', 'HospitalController@view')->name('view_hospital');
    
    Route::post('create-staff', 'UserController@createStaff')->name('create_staff');
    Route::get('get-user-detail', 'UserController@getUserDetail')->name('get_user_detail');
    Route::post('upate-staff', 'UserController@updateStaff')->name('update_staff');
    Route::post('delete-staff', 'UserController@deleteStaff')->name('delete_staff');
    

    Route::get('country-index', 'CountryController@index')->name('country_index');
    Route::get('country-update/{id}/{status}', 'CountryController@update')->name('country_update');   
    
    Route::get('city', 'CityController@list')->name('city_list');
    Route::get('create-city', 'CityController@create')->name('create_city');
    Route::post('create-city', 'CityController@create')->name('create_city');
    Route::get('edit-city/{id}', 'CityController@edit')->name('edit_city');
    Route::post('edit-city/{id}', 'CityController@edit')->name('edit_city');
    Route::post('delete-city', 'CityController@delete')->name('delete_city');

    Route::get('department', 'DepartmentController@list')->name('department_list');
    Route::get('create-department', 'DepartmentController@create')->name('create_department');
    Route::post('create-department', 'DepartmentController@create')->name('create_department');
    Route::get('edit-department/{id}', 'DepartmentController@edit')->name('edit_department');
    Route::post('edit-department/{id}', 'DepartmentController@edit')->name('edit_department');
    Route::post('delete-department', 'DepartmentController@delete')->name('delete_department');

    Route::get('id-type', 'IdTypeController@list')->name('id_type_list');
    Route::get('create-id-type', 'IdTypeController@create')->name('create_id_type');
    Route::post('create-id-type', 'IdTypeController@create')->name('create_id_type');
    Route::get('edit-id-type/{id}', 'IdTypeController@edit')->name('edit_id_type');
    Route::post('edit-id-type/{id}', 'IdTypeController@edit')->name('edit_id_type');
    Route::post('delete-id-type', 'IdTypeController@delete')->name('delete_id_type');
    
    Route::get('injection-site', 'InjectionSiteController@list')->name('injection_site_list');
    Route::get('create-injection-site', 'InjectionSiteController@create')->name('create_injection_site');
    Route::post('create-injection-site', 'InjectionSiteController@create')->name('create_injection_site');
    Route::get('edit-injection-site/{id}', 'InjectionSiteController@edit')->name('edit_injection_site');
    Route::post('edit-injection-site/{id}', 'InjectionSiteController@edit')->name('edit_injection_site');
    Route::post('delete-injection-site', 'InjectionSiteController@delete')->name('delete_injection_site');

    Route::get('region', 'RegionController@list')->name('region_list');
    Route::get('create-region', 'RegionController@create')->name('create_region');
    Route::post('create-region', 'RegionController@create')->name('create_region');
    Route::get('edit-region/{id}', 'RegionController@edit')->name('edit_region');
    Route::post('edit-region/{id}', 'RegionController@edit')->name('edit_region');
    Route::post('delete-region', 'RegionController@delete')->name('delete_region');


    Route::get('vaccine-manufacturer', 'VaccineManufacturerController@list')->name('vaccine_manufacturer_list');
    Route::get('create-vaccine-manufacturer', 'VaccineManufacturerController@create')->name('create_vaccine_manufacturer');
    Route::post('create-vaccine-manufacturer', 'VaccineManufacturerController@create')->name('create_vaccine_manufacturer');
    Route::get('edit-vaccine-manufacturer/{id}', 'VaccineManufacturerController@edit')->name('edit_vaccine_manufacturer');
    Route::post('edit-vaccine-manufacturer/{id}', 'VaccineManufacturerController@edit')->name('edit_vaccine_manufacturer');
    Route::post('delete-vaccine-manufacturer', 'VaccineManufacturerController@delete')->name('delete_vaccine_manufacturer');

    Route::get('vaccine-type', 'VaccineTypeController@list')->name('vaccine_type_list');
    Route::get('create-vaccine-type', 'VaccineTypeController@create')->name('create_vaccine_type');
    Route::post('create-vaccine-type', 'VaccineTypeController@create')->name('create_vaccine_type');
    Route::get('edit-vaccine-type/{id}', 'VaccineTypeController@edit')->name('edit_vaccine_type');
    Route::post('edit-vaccine-type/{id}', 'VaccineTypeController@edit')->name('edit_vaccine_type');
    Route::post('delete-vaccine-type', 'VaccineTypeController@delete')->name('delete_vaccine_type');
});
