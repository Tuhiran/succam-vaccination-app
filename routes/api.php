<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function () {
    Route::post('login', 'UserController@login');
    Route::post('match-otp', 'UserController@matchOtp');
    Route::post('resend-otp', 'UserController@resendOtp');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('vaccine-manufacturer-list', 'VaccineManufacturerController@vaccineManufacturerList');
        Route::get('injection-site-list', 'InjectionSiteController@injectionSiteList');
        Route::get('id-type-list', 'IdTypeController@idTypeList');
        Route::post('store-vaccination-record', 'VaccinationRecordController@storeVaccinationRecord');
    });
});
