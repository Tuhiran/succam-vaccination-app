<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    public function department() {
        return $this->hasOne('App\Model\Department', 'id', 'department_id');
    }

    public function departments() {
        return $this->hasMany('App\Model\Department', 'id', 'department_id');
    }

    public function city(){
        return $this->belongsTo('App\Model\City', 'city_id', 'id');
    }

    public function region(){
        return $this->belongsTo('App\Model\Region', 'region_id', 'id');
    }

    public function staffs() {
        return $this->hasMany('App\User', 'hospital_id', 'id');
    }
}
