<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VaccinationRecord extends Model
{
    protected $fillable = [
        'created_by_staff', 'id_type_id', 'id_type_detail', 'vaccine_name', 'vaccine_receiver_dob', 'gender','vaccination_dose','vaccine_manufacturer_id','vaccination_batch_no', 'injection_site_id','vaccine_manufacturing_date','is_contaminated_before', 'vaccination_date','vaccine_receiver_img'
    ];
}
