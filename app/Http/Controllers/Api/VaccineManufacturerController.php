<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\VaccineManufacturer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VaccineManufacturerController extends Controller {

    public function vaccineManufacturerList(Request $request) {
        if (Auth::guard('api')->check()) {
            $vaccineManufacturerList = VaccineManufacturer::where('status', '=', 1)->orderBy('name', 'ASC')->get();
            $response = [
                'status' => 'success',
                'vaccineManufacturerList' => $vaccineManufacturerList
            ];
        } else {
            $response = [
                'status' => 'error',
                'error' => 'authentication expired'
            ];
        }
        return response()->json($response);
    }

}
