<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\InjectionSite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InjectionSiteController extends Controller {

    public function injectionSiteList(Request $request) {
        if (Auth::guard('api')->check()) {
            $injectionSiteList = InjectionSite::where('status', '=', 1)->orderBy('name', 'ASC')->get();
            $response = [
                'status' => 'success',
                'injectionSiteList' => $injectionSiteList
            ];
        } else {
            $response = [
                'status' => 'error',
                'error' => 'authentication expired'
            ];
        }
        return response()->json($response);
    }

}
