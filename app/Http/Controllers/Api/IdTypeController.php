<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\IdType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IdTypeController extends Controller {

    public function idTypeList(Request $request) {
        if (Auth::guard('api')->check()) {
            $idTypeList = IdType::where('status', '=', 1)->orderBy('name', 'ASC')->get();
            $response = [
                'status' => 'success',
                'idTypeList' => $idTypeList
            ];
        } else {
            $response = [
                'status' => 'error',
                'error' => 'authentication expired'
            ];
        }
        return response()->json($response);
    }

}
