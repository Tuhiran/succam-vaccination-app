<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {

    public function login(Request $request) {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required',
                    'login_type' => 'required',
        ]);

        if ($validator->fails()) {
            $error['has_error'] = 1;
            $error['msg'] = $validator->errors();
            return response()->json($error, 401);
        }

        $error = array();
        $user = User::where([['email', $input['email']], ['status', 1]])->first();
        if (empty($user)) {
            $error['has_error'] = 1;
            $error['msg'] = "Credentials are not matched !!";
            return response()->json($error, 401);
        }

        if (Hash::check($input['password'], $user->password)) {
            $user->api_token = $user->id . Str::random(40);
            $user->login_type = isset($input['login_type']) ? $input['login_type'] : 'A';
            $user->fcm_token = isset($input['fcm_token']) ? $input['fcm_token'] : '';
            $user->device_id = isset($input['device_id']) ? $input['device_id'] : '';
            $user->save();
            if ($user->email_verified_at == null) {
                $otp = mt_rand(1000, 9999);
                $user->otp = $otp;
                $subject = 'User verification';
                $mailcontent = 'Hello, ' . $user->name . '<br>
                                Here is you verification OTP - ' . $otp;
                $sender_email = 'admin@admin.com';
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: <admin@admin.com>' . "\r\n";
                $to = $user->email;
                @mail($to, $subject, $mailcontent, $headers);
                $user->save();
                $response = [
                    'status' => 'success',
                    'msg' => 'Otp is send to your email. Please check your mail.',
                    'user' => $user,
                ];
            } else {
                $response = [
                    'status' => 'success',
                    'user' => $user,
                ];
            }

            return response()->json($response, 200);
        }
        $response = [
            'status' => 'error',
            'msg' => 'credential_mismatch'
        ];
        return response()->json($response, 401);
    }

    public function matchOtp(Request $request) {
        $response = array();
        $validator = Validator::make($request->all(), [
                    'otp' => 'required',
        ]);

        if ($validator->fails()) {
            $error['has_error'] = 1;
            $error['msg'] = $validator->errors();
            return response()->json($error);
        }
        $user = Auth::guard('api')->user();
        if ($user) {
            if ($user->otp == $request->otp) {
                $user->email_verified_at = date('Y-m-d H:i:s');
                $user->save();
                $response = [
                    'status' => 'success',
                    'msg' => 'OTP matched. Your email id has been verified.',
                    'user' => $user
                ];
                return response()->json($response, 200);
            } else {
                $response = [
                    'status' => 'error',
                    'msg' => 'OTP mismatch'
                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'msg' => 'User Not Found'
            ];
        }

        return response()->json($response, 200);
    }

    public function resendOtp(Request $request) {
        $response = array();
        $user = Auth::guard('api')->user();
        if ($user) {
            $otp = mt_rand(1000, 9999);
            $user->otp = $otp;
            $user->save();
            $subject = 'User verification';
            $mailcontent = 'Hello, ' . $user->name . '<br>
                            Here is your new verification OTP - ' . $otp;
            $sender_email = 'admin@admin.com';
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: <admin@admin.com>' . "\r\n";
            $to = $user->email;
            @mail($to, $subject, $mailcontent, $headers);
            $response = [
                'status' => 'success',
                'msg' => 'Otp is send to your email. Please check your mail.',
                'user' => $user,
            ];
            return response()->json($response, 200);
        } else {
            $response = [
                'status' => 'error',
                'msg' => 'User Not Found'
            ];
            return response()->json($response, 401);
        }
    }

}
