<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\VaccinationRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VaccinationRecordController extends Controller {
    
    public function storeVaccinationRecord(Request $request){
        if (Auth::guard('api')->check()) {
            $this->validate($request, [
                'id_type_id' => 'required',
                'id_type_detail' => 'required',
                'vaccine_name' => 'required',
                'vaccine_receiver_dob' => 'required',
                'gender' => 'required',
                'vaccination_dose' => 'required',
                'vaccine_manufacturer_id' => 'required',
                'vaccination_batch_no' => 'required',
                'injection_site_id' => 'required',
                'vaccine_manufacturing_date' => 'required',
                'is_contaminated_before' => 'required',
                'vaccination_date' => 'required',
            ]);
            $user = Auth::guard('api')->user();
            $vaccinationRecord = VaccinationRecord::create([
                'created_by_staff' => $user->id,
                'id_type_id' => $request->id_type_id,
                'id_type_detail' => $request->id_type_detail,
                'vaccine_name' => $request->vaccine_name,
                'vaccine_receiver_dob' => date('Y-m-d', strtotime($request->vaccine_receiver_dob)),
                'gender' => $request->gender,
                'vaccination_dose' => $request->vaccination_dose,
                'vaccine_manufacturer_id' => $request->vaccine_manufacturer_id,
                'vaccination_batch_no' => $request->vaccination_batch_no,
                'injection_site_id' => $request->injection_site_id,
                'vaccine_manufacturing_date' => strtotime($request->vaccine_manufacturing_date),
                'is_contaminated_before' => $request->is_contaminated_before,
                'vaccination_date' => strtotime($request->vaccination_date),
            ]);

            if(!empty($request->vaccine_receiver_img)) {
                $img = $request->vaccine_receiver_img;
                $name = time() . '.' . $img->getClientOriginalExtension();
                $img->move(public_path() . '/uploads/vaccine_reciver_img', $name);
                $vaccine_receiver_img = $name;
            } else {
                $vaccine_receiver_img = NULL;
            }
            $vaccinationRecord->vaccine_receiver_img = $vaccine_receiver_img;
            $vaccinationRecord->save();
            if($vaccinationRecord){
                $response = [
                    'status' => 'success',
                    'msg' => 'Recored Saved successfully',
                    'vaccinationRecord' => $vaccinationRecord
                ];
            } else {

            }
        } else {
            $response = [
                'status' => 'error',
                'error' => 'authentication expired'
            ];
        }
        return response()->json($response);
    }
}
