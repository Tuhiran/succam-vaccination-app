<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\IdType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class IdTypeController extends Controller {

    public function list(Request $request) {
        if (Auth::guard('admins')->check()) {
            $idTypeList = IdType::where('status', '!=', 2)->orderBy('id', 'DESC')->get();

            return view('admin.id_types.index', compact('idTypeList'));
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function create(Request $request) {
        if (Auth::guard('admins')->check()) {
            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'name' => 'required',
                    'status' => 'required',
                ]);
                $input = $request->all();
                /* $queryData = DB::select(DB::raw("SELECT * FROM id_types WHERE REPLACE(LOWER(name),' ','') LIKE '%" . str_replace(' ', '', strtolower($input['name'])) . "%'"));
                  if (count($queryData) == 0) { */
                $model = new IdType();
                $model->created_by = Auth::guard('admins')->id();
                $model->name = $input['name'];
                $model->status = $input['status'];
                $model->save();
                return redirect()->route('id_type_list');
                /* } else {
                  return redirect()->route('create_id_type')->with('error', $queryData[0]->name . ' Id Type already exist.');
                  } */
            } else {
                return view('admin.id_types.create');
            }
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function edit(Request $request, $id) {
        if (Auth::guard('admins')->check()) {
            $input = $request->all();
            $model = IdType::find($id);
            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'name' => 'required',
                    'status' => 'required',
                ]);
                $queryData = DB::select(DB::raw("SELECT * FROM id_types WHERE REPLACE(LOWER(name),' ','') LIKE '" . str_replace(' ', '', strtolower($input['name'])) . "' AND id != " . $id . ""));
                if (count($queryData) == 0) {
                    $model->name = $input['name'];
                    $model->status = $input['status'];
                    $model->save();
                    return redirect()->route('id_type_list');
                } else {
                    return redirect()->route('edit_id_type', ['id' => $id])->with('error', $queryData[0]->name . ' Id Type already exist.');
                }
            }
            return view('admin.id_types.update', compact('model'));
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function delete(Request $request) {
        if (Auth::check()) {
            $input = $request->all();
            $model = IdType::find($input['id']);
            $model->status = 2;
            $model->save();
            return response()->json(array('success' => true, 'url' => route('id_type_list')));
        } else {
            return redirect()->route('admin-login');
        }
    }

}
