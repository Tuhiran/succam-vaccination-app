<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\City;
use App\Model\Department;
use App\Model\Hospital;
use App\Model\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HospitalController extends Controller {

    public function list(Request $request) {
        if (Auth::guard('admins')->check()) {

            $hospitalList = Hospital::where('status', '!=', 2)->orderBy('id', 'DESC')->get();
            return view('admin.hospitals.index', compact('hospitalList'));
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function create(Request $request) {
        if (Auth::guard('admins')->check()) {
            $cityList = City::where('status', 1)->orderBy('name', 'ASC')->get();
            $departmentList = Department::where('status', 1)->orderBy('name', 'ASC')->get();
            $regionList = Region::where('status', 1)->orderBy('name', 'ASC')->get();
            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'name' => 'required',
                    'status' => 'required',
                ]);
                $input = $request->all();
                /* $queryData = DB::select(DB::raw("SELECT * FROM hospitals WHERE REPLACE(LOWER(name),' ','') LIKE '%" . str_replace(' ', '', strtolower($input['name'])) . "%'"));
                  if (count($queryData) == 0) { */
                $model = new Hospital();
                $model->name = $input['name'];
                $model->region_id = $input['region_id'];
                $model->department_id = $input['department_id'];
                $model->city_id = $input['city_id'];
                $model->status = $input['status'];
                $model->save();
                return redirect()->route('hospital_list');
                /* } else {
                  return redirect()->route('create_hospital')->with('error', $queryData[0]->name . ' city already exist.');
                  } */
            } else {
                return view('admin.hospitals.create', compact('cityList', 'departmentList', 'regionList'));
            }
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function edit(Request $request, $id) {
        if (Auth::guard('admins')->check()) {
            $input = $request->all();
            $model = Hospital::find($id);
            $cityList = City::where('status', 1)->orderBy('name', 'ASC')->get();
            $departmentList = Department::where('status', 1)->orderBy('name', 'ASC')->get();
            $regionList = Region::where('status', 1)->orderBy('name', 'ASC')->get();
            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'name' => 'required',
                    'status' => 'required',
                ]);
                /* $queryData = DB::select(DB::raw("SELECT * FROM hospitals WHERE REPLACE(LOWER(name),' ','') LIKE '" . str_replace(' ', '', strtolower($input['name'])) . "' AND id != " . $id . ""));
                  if (count($queryData) == 0) { */
                $model->name = $input['name'];
                $model->region_id = $input['region_id'];
                $model->department_id = $input['department_id'];
                $model->city_id = $input['city_id'];
                $model->status = $input['status'];
                $model->save();
                return redirect()->route('hospital_list');
                /* } else {
                  return redirect()->route('edit_city', ['id' => $id])->with('error', $queryData[0]->name . ' city already exist.');
                  } */
            }
            return view('admin.hospitals.update', compact('model', 'cityList', 'departmentList', 'regionList'));
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function view(Request $request, $id) {
        if (Auth::guard('admins')->check()) {
            $model = Hospital::with(['staffs' => function($query) {
                            $query->where('status', '!=', 2);
                        }])->find($id);
            return view('admin.hospitals.view', compact('model'));
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function deleteStaff(Request $request) {
        if (Auth::guard('admins')->check()) {
            $input = $request->all();
            $model = Hospital::find($input['id']);
            $model->status = 2;
            $model->save();
            return response()->json(array('success' => true, 'url' => route('hospital_list')));
        } else {
            return redirect()->route('admin-login');
        }
    }

}
