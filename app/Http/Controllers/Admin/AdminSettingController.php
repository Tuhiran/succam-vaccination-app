<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\AdminSetting;
use Illuminate\Support\Str;

class AdminSettingController extends Controller {

    public function index(Request $request) {
        if (Auth::check()) {
            $modelData = AdminSetting::orderBy('id', 'DESC')->get();
            return view('admin.admin_setting.index', compact('modelData'));
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function create(Request $request) {
        if (Auth::check()) {
            if ($request->isMethod('post')) {
                $input = $request->all();
                $model = new AdminSetting();
                $model->field_name = $input['field_name'];
                $model->field_type = $input['field_type'];
                if ($input['field_type'] == "F") {
                    $file = $input['file_type_field_value'];
                    $fileName = time() . Str::random(10) . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path() . '/assets/upload/introductory_video/', $fileName);
                    $videoThumbnail = $input['video_thumbnail'];
                    $videoThumbnailName = time() . Str::random(10) . '.' . $videoThumbnail->getClientOriginalExtension();
                    $videoThumbnail->move(public_path() . '/assets/upload/introductory_video/', $videoThumbnailName);
                    $model->video_thumbnail = $videoThumbnailName;
                }
                $model->field_value = $input['field_type'] == "I" ? $input['input_type_field_value'] : $fileName;

                $model->save();
                return redirect()->route('admin_setting_index');
            } else {
                return view('admin.admin_setting.create');
            }
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function update(Request $request, $id) {
        if (Auth::check()) {
            $model = AdminSetting::find($id);
            $fileName = $model->field_value;
            if ($request->isMethod('post')) {
                $input = $request->all();
                if ($input['field_type'] == "F") {
                    if (array_key_exists('file_type_field_value', $input)) {
                        $file = $input['file_type_field_value'];
                        if ($file != '') {
                            unlink(public_path() . '/assets/upload/introductory_video/' . $model->field_value);
                            $fileName = time() . Str::random(10) . '.' . $file->getClientOriginalExtension();
                            $file->move(public_path() . '/assets/upload/introductory_video/', $fileName);
                        }
                    }
                    if (array_key_exists('video_thumbnail', $input)) {
                        $videoThumbnail = $input['video_thumbnail'];
                        if ($videoThumbnail != '') {
                            unlink(public_path() . '/assets/upload/introductory_video/' . $model->video_thumbnail);
                            $videoThumbnailName = time() . Str::random(10) . '.' . $videoThumbnail->getClientOriginalExtension();
                            $videoThumbnail->move(public_path() . '/assets/upload/introductory_video/', $videoThumbnailName);
                            $model->video_thumbnail = $videoThumbnailName;
                        }
                    }
                }
                $model->field_value = $input['field_type'] == "I" ? $input['input_type_field_value'] : $fileName;
                $model->save();
                return redirect()->route('admin_setting_index');
            } else {

                return view('admin.admin_setting.update', compact('model'));
            }
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function view(Request $request, $id) {
        if (Auth::check()) {
            $model = $model = AdminSetting::find($id);
            return view('admin.admin_setting.view', compact('model'));
        } else {
            return redirect()->route('admin-login');
        }
    }

}
