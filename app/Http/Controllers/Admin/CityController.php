<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller {

    public function list(Request $request) {
        if (Auth::guard('admins')->check()) {
            $cityList = City::where('status', '!=', 2)->orderBy('id', 'DESC')->get();

            return view('admin.cities.index', compact('cityList'));
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function create(Request $request) {
        if (Auth::guard('admins')->check()) {
            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'name' => 'required|name',
                    'status' => 'required',
                ]);
                $input = $request->all();
                /* $queryData = DB::select(DB::raw("SELECT * FROM `cities` WHERE REPLACE(LOWER(name),' ','') LIKE '%" . str_replace(' ', '', strtolower($input['name'])) . "%'"));
                  if (count($queryData) == 0) { */
                $model = new City();
                $model->created_by = Auth::guard('admins')->id();
                $model->name = $input['name'];
                $model->status = $input['status'];
                $model->save();
                return redirect()->route('city_list');
                /* } else {
                  return redirect()->route('create_city')->with('error', $queryData[0]->name . ' city already exist.');
                  } */
            } else {
                return view('admin.cities.create');
            }
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function edit(Request $request, $id) {
        if (Auth::guard('admins')->check()) {
            $input = $request->all();
            $model = City::find($id);
            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'name' => 'required',
                    'status' => 'required',
                ]);
                /* $queryData = DB::select(DB::raw("SELECT * FROM cities WHERE REPLACE(LOWER(name),' ','') LIKE '" . str_replace(' ', '', strtolower($input['name'])) . "' AND id != " . $id . ""));
                  if (count($queryData) == 0) { */
                $model->name = $input['name'];
                $model->status = $input['status'];
                $model->save();
                return redirect()->route('city_list');
                /* } else {
                  return redirect()->route('edit_city', ['id' => $id])->with('error', $queryData[0]->name . ' city already exist.');
                  } */
            }
            return view('admin.cities.update', compact('model'));
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function delete(Request $request) {
        if (Auth::check()) {
            $input = $request->all();
            $model = City::find($input['id']);
            $model->status = 2;
            $model->save();
            return response()->json(array('success' => true, 'url' => route('city_list')));
        } else {
            return redirect()->route('admin-login');
        }
    }

}
