<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Validation\Rule;
use View;

class UserController extends Controller {

    public function getUserList(Request $request) {
        if (Auth::check()) {
            $userList = User::where('user_type', 'H')->orderBy('id', 'DESC')->get();

            return view('admin.users.index', compact('userList'));
        } else {
            return redirect()->route('admin-login');
        }
    }

    public function getUserDetail(Request $request) {
        if (Auth::guard('admins')->check()) {
            $userDetail = User::find($request->id);
            $response = [
                'status' => 'success',
                'user' => $userDetail
            ];
        } else {
            $response = [
                'status' => 'error',
                'error' => 'authentication_expired'
            ];
        }
        return response()->json($response);
    }

    public function createStaff(Request $request) {
        if (Auth::guard('admins')->check()) {
            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'name' => 'required',
                    'email' => ['required', 'string', 'email', 'max:225', Rule::unique('users')->where(function ($query) use ($request) {
                            return $query->where('status', 1);
                        })],
                    'password' => 'required',
                ]);
                $input_data = array();
                $input_data['hospital_id'] = $request->hospital;
                $input_data['name'] = $request->name;
                $input_data['email'] = $request->email;
                $input_data['pin'] = $request->password;
                $input_data['user_type'] = 1;  /* 1 => Hospital Staff */
                $input_data['password'] = bcrypt($request->password);
                $input_data['status'] = $request->status;
                User::create($input_data);
                $response = [
                    'status' => 'success',
                    'msg' => 'staff_created'
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'msg' => 'method_error'
                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'msg' => 'authentication_expired'
            ];
        }
        return response()->json($response);
    }

    public function updateStaff(Request $request) {
        $response = [];
        if (Auth::check()) {
            $user = User::find($request->staff);
            if (empty($user)) {
                return redirect()->route('view_hospital', ['id' => $request->hospital]);
            }
            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'name' => 'required',
                    'password' => 'required',
                ]);
                if ($user->email == $request->email) {
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->pin = $request->password;
                    $user->password = bcrypt($request->password);
                    $user->save();
                    $response = [
                        'status' => 'success',
                        'msg' => 'staff_updated'
                    ];
                } else {
                    $this->validate($request, [
                        'email' => ['required', 'string', 'email', 'max:225', Rule::unique('users')->where(function ($query) use ($request) {
                                return $query->where('status', 1);
                            })]
                    ]);
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->pin = $request->password;
                    $user->password = bcrypt($request->password);
                    $user->save();
                    $response = [
                        'status' => 'success',
                        'msg' => 'staff_updated'
                    ];
                }
            } else {
                $response = [
                    'status' => 'error',
                    'msg' => 'method_error'
                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'msg' => 'authentication_expired'
            ];
        }
        return response()->json($response);
    }

    public function deleteStaff(Request $request) {
        if (Auth::check()) {
            $userDetail = User::find($request->id);
            $userDetail->status = 2; /* 0=>Inactive, 1=>Active, 2=>Deleted */
            $userDetail->save();
            $response = [
                'status' => 'success',
            ];
        } else {
            $response = [
                'status' => 'error',
                'error' => 'authentication_expired'
            ];
        }
        return response()->json($response);
    }

}
