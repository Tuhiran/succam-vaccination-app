<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Admin;

class LoginController extends Controller {

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Where to redirect users after logout.
     *
     * @var string
     */
    protected $redirectAfterLogout = '/admin';

    public function __construct() {

        $this->middleware('guest.admin', ['except' => 'logout']);
    }

    public function loginPage() {
        return view('admin.auth.login');
    }

    public function adminLogin(Request $request) {
        $this->validateLogin($request);

        $user = Admin::where([['email', $request['email']]])->first();
        if ($user != '') {
            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }

            $this->incrementLoginAttempts($request);
            return $this->sendFailedLoginResponse($request);
        } else {
            return $this->sendFailedLoginResponse($request);
        }
    }

    public function logout(Request $request) {
        if (!empty(Auth::guard('admins')->user())) {
            $this->guard()->logout();
            $request->session()->flush();
            $request->session()->regenerate();
            return redirect($this->redirectAfterLogout);
        } else {
            return redirect('/admin');
        }
    }

    public function getDashboard(Request $request) {
        $input = $request->all();
        return view('dashboard');
    }

}
